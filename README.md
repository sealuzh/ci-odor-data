This is the replication package for the paper "Automated Reporting of Anti-Patterns and Decay in Continuous Integration".

Our replication package includes two repositories and one docker container image:

- _ci-odor-scripts_ (https://bitbucket.org/sealuzh/ci-odor-scripts, tag V1) containing our complete set of scripts (with a subset of the input data);
- _ci-odor-data_ (https://bitbucket.org/sealuzh/ci-odor-data, tag V1) containing our full set of data;
- _docker container image_ (https://hub.docker.com/r/carminevassallo/ci-odor, tag V3) containing all the scripts (and input data) in a pre-configured environment.

We describe all the steps needed in order to replicate the results presented in our paper. 
This description is focused on the _docker container image_.
The _ci-odor-data_ contains additional material we refer to during the descriptions of the steps and at the end.
To keep the time of the artifact evaluation within a reasonable time (30 minutes, as suggested), we provide reduced input data for some steps that otherwise will last hours. 


# Set-up of the Docker Image Container 

- Install Docker for Desktop (https://www.docker.com/products/docker-desktop)
- Once docker is correctly installed, load our image from dockerhub executing in the terminal:
```sh
docker pull carminevassallo/ci-odor:v3
```
- Then execute: 
```sh
docker run -t -i carminevassallo/ci-odor:v3 /bin/bash
```
- Now you are in the container and can move to the home directory of our artifact:
```sh
cd artifact-home/
```    
Remember that from now on you are required to have "artifact-home/" as your working directory, i.e., it includes the presented scripts.

# Reporting CI Practices

In this section, we describe how to run our anti-patterns detection tool. 
We start introducing the scripts used for our projects selection.
Then, we describe how to extract all the projects data, i.e., repository, build history, and logs.
Finally, we illustrate how to generate anti-patterns reports for the considered projects and how to quantify their occurrence.

Because of the amount of time required for some of the steps, we mainly focus on the following GitHub projects to generate input data for our steps:
- google/closure-compiler
- spring-cloud/spring-cloud-aws
- geotools/geotools
- speedment/speedment

However, in our _ci-odor-data_ you can find the full version of reduced input data in the folder "input_data". Furthermore, we also include in "output_data" all the data deriving from the execution of the scripts having as input the full dataset.

## Projects Selection 
This step requires internet connection.
```sh
./selection.sh
```

The script takes as input the list of projects ("projectsList.csv") returned from the query on GHTorrent (see _Notes_). The expected output is "selection_output.csv" that contains the list of projects with _isMaven_ (i.e., is a maven project) and _isTravis_ (i.e., is hosted on Travis) columns. 
We manually filtered out projects that have both _isMaven_ and _isTravis_ set to true.

## Projects Data Extraction
This step requires internet connection.
```sh
./extraction.sh
```

The script takes as input (i) the list ("projectsListOnlyName.csv") of projects (only the name) that is derived from the previous step and (ii) a configuration file ("config/config.yaml") for the log parsing. As output it creates a folder for each project in "projects_data/" and (i) clones the repository in "repo/", (ii) extracts the build history in "builds/", and (iii) download all the build logs necessary for computing the skip tests anti-pattern later in "logs/".

Note that this step is very time-consuming. Thus, we include only one project in "projectsListOnlyName.csv".

## Anti-Patterns Detection

```sh
./detection.sh
```

The script takes as input the list of projects ("projectsListWithMaster.csv") that the user intends to inspect for the presence of CI anti-patterns. This list contains an additional column (compared to "projectsListOnlyName.csv") indicating the name of the release branch that we manually inspected from the corresponding available information on GitHub. The other inputs are (i) "projects/" that contains projects data extracted in the previous step and (ii) a configuration file ("config/config.yaml") for the log parsing.
As output, our tool produces in "detected_smells/" a subfolder for each analyzed projects containing a file for each anti-pattern type.

## Generation of HTML Reports

```sh
./reporting.sh
```

The script takes as input (in "reportGeneration" folder) project folders containing a file for each anti-pattern type (the subfolders of "detected_smells/" in the previous step).
As output, the script produces in the "out/" folder html reports for each project (you can start navigating them from "index.html").

Note1: if you want to browse the results since the container does not provide a web browser, you must move them on your local machine. 
- Create a tar archive from the "out/" directory:
```sh
cd reportGeneration
tar cvzf out.tgz out
```   
- From your host machine identify the Docker image id
```sh
docker ps
```
You will see the id in the first column near the image name, e.g.
> CONTAINER ID        IMAGE                        COMMAND             CREATED             STATUS              PORTS               NAMES
> aab2bc8a52fa        carminevassallo/ci-odor:v3   "/bin/bash"         18 minutes ago      Up 18 minutes                           recursing_tereshkova

- from your host machine, copy the tar archive (replace  "aab2bc8a52fa" with your container ID)
```sh
docker cp aab2bc8a52fa:/artifact_home/reportGeneration/out.tgz .
```
- uncompress the archive
```sh
tar xvzf out.tgz
```
then you can browse it starting from the index.html file

Note2: Results shown in our report depends on the actual date of the reports generation. For example, in the skip tests section, our tool will show the tests that have been skipped in the last 6 months from today.

## Quantification of the phenomenon

```sh
./quantification.sh
```

The script takes as input (in "reportGeneration" folder)  project folders containing a file for each anti-pattern type (the subfolders of "detected_smells/" in the previous step).
As output, the script produces in the "PureDetection/" folder including information for each anti-pattern.

## Notes

Our project selection starts with queries performed on GHTorrent. The queries script is available in our _ci-odor-data_ ("queryOnGHTorrent.txt" file).

# Survey Data Processing
We now describe the usage of the scripts that we have generated for processing data from our surveys.

## Scripts for pre-processing raw results

We cannot share the raw data that we have collected in the two surveys, because it contains sensitive data. 
However, we are sharing our preprocessing scripts and the resulting data. 
In _ci-odor-scripts_ you will find two folders for the surveys, "preprocessing-first" and "preprocessing-second". 
We have started the runme.py files to create the two files "survey1-preprocessed.csv" and "survey2-preprocessed.csv" that you will find in the root of the repository. Please note again that you cannot run the preprocessing yourself, because we could not include the original files.

## Script for generating the agreement results

The paper includes two Figures that show the agreement of our participants to several questions (Figures 3+8). You can generate these figures yourself, by running the two corresponding "runme.py" files that are contained in the two folders "agreement-fig3" and "agreement-fig8".
For example:
```sh
cd agreement-fig3 (or ...-fig9)
python3 runme.py
```
Each script will generate a file named "agreement-figX.pdf" in the root of the repository.

## Kruskal-Wallis test

This script uses the Kruskal-Wallis test to determine whether any of the antipatterns (all 9  considered in the first part of the study, including those we discarded afterward) is more relevant for a particular programming language than another. Run it using
```sh
./kruskal.sh
```

under the "kruskal/" directory you will find the file "kruskal_analysis.csv" containing the results of the analysis. In particular, the last column reports the p-value for each antipattern (as you can see always >0.05).

## Wilcoxon rank sum test
```sh
./wilcoxon.sh
```

The script takes as input the file "survey2-preprocessed-transform.csv". It contains results of the post-study survey, in which the levels of agreement are encoded in a scale from 1 (strongly disagree) to 5 (strongly agree). The column "has_seen_report" indicates whether the respondent has previously seen the antipattern in his/her project.
It produces results in the file "wilcoxon/wilcoxon_output.csv". For each question, the p-value (last column of the generated csv) indicates whether there is a significant difference in the evaluation of developers that have previously seen the antipattern in their project an those who did not. As you can see, results indicate that in all cases there are no statistically-significant differences.

# Other data 

In _ci-odor-data_ we also include the following items.

- "cardSorting/" folder containing the results of our card sorting sessions
- "contactedProjects.csv": Contacted projects with corresponding public available communication channels
- "survey1/2.pdf": Full export of both survey
- "survey1/2-preprocessed.csv": Full export of our collected data (good column names, anonymized answers).

# Prerequisites for running our artifact (already set-up in the provided image)

- Java version: Java SE 8
- Perl version: v5.26.1
- R version: 3.4.4 (2018-03-15)
- Required R Packages:
    - knitr
    - dplyr
    - kableExtra
    - htmltools
    - lubridate
    - ScottKnott
- Set RSTUDIO_PANDOC to "pandoc" directory (e.g., RSTUDIO_PANDOC=/usr/lib/rstudio/bin/pandoc)
- Python version: 3
- Required Python packages (available via pip3)
    - pandas
    - matplotlib
